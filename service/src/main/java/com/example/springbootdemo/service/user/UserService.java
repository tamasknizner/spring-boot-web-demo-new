package com.example.springbootdemo.service.user;

import java.util.List;

import com.example.springbootdemo.service.user.domain.User;

public interface UserService {

    User save(User user);

    User findById(Long id);

    List<User> findAll();

}
