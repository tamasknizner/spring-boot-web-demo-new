package com.example.springbootdemo.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.springbootdemo.service.user.UserService;
import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.service.user.domain.exception.UserNotFoundException;
import com.example.springbootdemo.web.transformer.UserViewTransformer;
import com.example.springbootdemo.web.view.UserView;

@RestController
@RequestMapping("/rest")
public class RestUserController {

    private final UserViewTransformer userTransformer;
    private final UserService userService;

    public RestUserController(UserViewTransformer userTransformer, UserService userService) {
        this.userTransformer = userTransformer;
        this.userService = userService;
    }

    @GetMapping(path = "/users", produces = "application/json")
    public ResponseEntity<List<User>> users() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping(path = "/signup", consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> signup(@Valid @RequestBody UserView user, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, result.getFieldErrors().toString());
        }
        User savedUser = userService.save(userTransformer.transformView(user));
        return ResponseEntity.ok(savedUser);
    }

    @GetMapping(path = "/user/{id}", produces = "application/json")
    public ResponseEntity<User> userDetails(@PathVariable Long id) {
        User user;
        try {
            user = userService.findById(id);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return ResponseEntity.ok(user);
    }

}
