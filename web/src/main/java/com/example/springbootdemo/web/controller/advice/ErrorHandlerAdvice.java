package com.example.springbootdemo.web.controller.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.springbootdemo.service.user.domain.exception.UserNotFoundException;
import com.example.springbootdemo.web.controller.UserController;

@ControllerAdvice(basePackageClasses = UserController.class)
public class ErrorHandlerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandlerAdvice.class);

    @ExceptionHandler(UserNotFoundException.class)
    public String handleError(Exception exception) {
        LOGGER.error("Something went wrong: {}", exception.getMessage());
        return "custom_error_page";
    }

}
