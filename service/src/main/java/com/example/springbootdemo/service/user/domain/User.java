package com.example.springbootdemo.service.user.domain;

import java.time.LocalDateTime;

public record User(
        Long id,
        String name,
        String email,
        LocalDateTime createdDate,
        LocalDateTime lastModified) {
}
