package com.example.springbootdemo.it;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.StreamUtils;

import com.example.springbootdemo.SpringBootDemoApplication;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SpringBootDemoApplication.class)
@AutoConfigureMockMvc
//@WebMvcTest
public class FindUserByIdTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql(value = "classpath:test-data.sql")
    public void testFindUserByIdShouldReturnUserWithStatusOKWheCalledWithIdPresentInTheDatabase() throws Exception {
        // GIVEN

        // WHEN
        MvcResult mvcResult = mockMvc.perform(get("/rest/user/1")).andReturn();

        // THEN
        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        JSONAssert.assertEquals(expected("user_by_id"), mvcResult.getResponse().getContentAsString(), true);
    }

    @Test
    public void testFindUserByIdShouldReturnStatusNotFoundWheCalledWithIdNotPresentInTheDatabase() throws Exception {
        // GIVEN

        // WHEN
        MvcResult mvcResult = mockMvc.perform(get("/rest/user/2")).andReturn();

        // THEN
        assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
    }

    private String expected(String fileName) throws IOException {
        return StreamUtils.copyToString(new ClassPathResource("expected/" + fileName + ".json").getInputStream(), StandardCharsets.UTF_8);
    }

}
