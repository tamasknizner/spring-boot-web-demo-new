package com.example.springbootdemo.web.transformer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.web.view.UserView;

@Component
public class UserViewTransformer {

    public UserView transform(User user) {
        return new UserView(user.id(), user.name(), user.email(), formatDate(user.createdDate()));
    }

    public User transformView(UserView user) {
        return new User(user.id(), user.name(), user.email(), null, null);
    }

    public List<UserView> transform(List<User> users) {
        return users.stream()
                .map(this::transform)
                .collect(Collectors.toList());
    }

    private String formatDate(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
}
