package com.example.springbootdemo.service.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.service.user.domain.exception.UserNotFoundException;
import com.example.springbootdemo.service.user.transformer.UserTransformer;
import com.example.springbootdemo.userdao.UserDao;
import com.example.springbootdemo.userdao.entity.UserEntity;

/*
Unit testing
- 1 test class / unit -> usually a single class
- public methods should be tested
- each condition, flow control should be tested (~ 1 test / condition, branch)
- Given / When / Then structure (or 3 As -> Arrange, Act, Assert)
- Method name: test<method name>should<expected outcome>when<condition>
- amit tesztelsz, hívd "underTest"-nek
- teszt kimenete mindig legyen "actual"

FIRST principles
- Fast -> yes
- Isolated / Independent -> mocks
- Repeatable -> deterministic outcome
- Self-validating -> use asserts, test should have a trivial result
- Timely / Thorough -> try to cover every scenario and not just aim for 100% code coverage

Dependency substitution object types:
- dummy: van
- stub: fixen egy reponse-t tud visszaadni mindentől függetlenül
- mock: adott paraméterre ad vissza adott választ
- spy: ennél tudod validálni, hogy mivel lett meghívva

 */

@ExtendWith(MockitoExtension.class)
class DefaultUserServiceTest {
    private static final UserEntity USER_ENTITY = new UserEntity();
    private static final Long USER_ID = 1L;
    private static final User USER = new User(USER_ID, null, null, null, null);
    private static final String EXPECTED_EXCEPTION_MESSAGE = "User not found with id 1";

    @Mock
    private UserDao userDao;

    @Mock
    private UserTransformer userTransformer;

    @InjectMocks
    private DefaultUserService underTest;

    @Test
    void testFindByIdShouldReturnUserWhenAvailable() {
        // GIVEN
        when(userDao.findById(USER_ID)).thenReturn(Optional.of(USER_ENTITY));
        when(userTransformer.transform(USER_ENTITY)).thenReturn(USER);

        // WHEN
        User actual = underTest.findById(USER_ID);

        // THEN
        assertEquals(USER, actual);
        verify(userDao).findById(USER_ID);
        verify(userTransformer).transform(USER_ENTITY);
        verifyNoMoreInteractions(userDao, userTransformer);
    }

    @Test
    void testFindByIdShouldThrowExceptionWhenDaoReturnsEmptyOptional() {
        // GIVEN
        when(userDao.findById(USER_ID)).thenReturn(Optional.empty());

        // WHEN
        UserNotFoundException actual = assertThrows(UserNotFoundException.class, () -> underTest.findById(USER_ID));

        // THEN
        assertEquals(EXPECTED_EXCEPTION_MESSAGE, actual.getMessage());
        verify(userDao).findById(USER_ID);
        verifyNoMoreInteractions(userDao);
        verifyNoInteractions(userTransformer);
    }

}