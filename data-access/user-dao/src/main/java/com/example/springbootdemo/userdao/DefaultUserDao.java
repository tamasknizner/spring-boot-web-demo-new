package com.example.springbootdemo.userdao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.example.springbootdemo.userdao.entity.UserEntity;
import com.example.springbootdemo.userdao.repository.UserRepository;

@Component
public class DefaultUserDao implements UserDao {

    private final UserRepository userRepository;

    DefaultUserDao(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity save(UserEntity user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public Optional<UserEntity> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }
}
